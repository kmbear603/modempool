﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModemPool2
{
    internal class ATCommand
    {
        private string _Cmd = "";

        public ATCommand(string cmd)
        {
            _Cmd = cmd;
        }

        public ATCommand()
        {
        }

        public ATCommand Append(string str)
        {
            _Cmd += str;
            return this;
        }

        public ATCommand Append(char ch)
        {
            _Cmd += ch;
            return this;
        }

        public static implicit operator ATCommand(string str)
        {
            return new ATCommand(str);
        }

        public string CommandString
        {
            get { return _Cmd; }
        }
    }
}
