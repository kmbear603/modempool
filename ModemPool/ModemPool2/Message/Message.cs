﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModemPool2.Message
{
    public abstract class Message
    {
        public Message()
        {
        }

        internal void Set(int id, string sender_or_receiver, DateTime time, string content)
        {
            Id = id;
            SenderOrReceiver = sender_or_receiver;
            Time = time;
            Content = content;
        }

        public int Id { get; private set; }
        protected string SenderOrReceiver { get; private set; }
        protected DateTime Time { get; private set; }
        public string Content { get; private set; }
    }
}
