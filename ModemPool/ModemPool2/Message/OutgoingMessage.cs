﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModemPool2.Message
{
    public abstract class OutgoingMessage : Message
    {
        public string Receiver
        {
            get { return SenderOrReceiver; }
        }

        public DateTime SendTime
        {
            get { return Time; }
        }
    }
}
