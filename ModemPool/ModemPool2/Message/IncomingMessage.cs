﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModemPool2.Message
{
    public abstract class IncomingMessage : Message
    {
        public string Sender
        {
            get { return SenderOrReceiver; }
        }

        public DateTime ReceiveTime
        {
            get { return Time; }
        }
    }
}
