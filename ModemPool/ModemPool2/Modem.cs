﻿#define USEGSMMODEM
#define SMSPDULIB
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Text.RegularExpressions;

namespace ModemPool2
{
    public class Modem
    {
        public enum MessageFormat_E { PDU, Text };
        public enum MessageCharset_E { UCS2, ISO8859, IRA, PCCP437, HEX };
        public enum MessageStatus_E
        {
            Unread, Read, Unsent, Sent, All
        }
        private class Result
        {
            public Result(string status, params string[] content)
            {
                Status = status;
                ContentArray = content;
            }

            public string Status
            {
                get; private set;
            }

            public string Content
            {
                get { return string.Join(LineSeparator, ContentArray); }
            }

            public string[] ContentArray
            {
                get; private set;
            }

            public string GetOrThrow()
            {
                if (Status != "OK")
                    throw new Exception("failed: " + Status);
                return Content;
            }

            public string[] GetAllOrThrow()
            {
                if (Status != "OK")
                    throw new Exception("failed: " + Status);
                return ContentArray;
            }

            public static implicit operator string(Result r)
            {
                return r.GetOrThrow();
            }

            public static implicit operator string[] (Result r)
            {
                return r.GetAllOrThrow();
            }
        }

        public delegate void OnReceivedSMS_D(Modem modem, int sms_id);

#if USEGSMMODEM
        private const string LineSeparator = "\r";
        private GSMMODEM.GSMModem _Modem = null;
        public event OnReceivedSMS_D OnReceivedSMS = null;

        public Modem()
        {
            _Modem = new GSMMODEM.GSMModem();
            _Modem.OnReceivedMessage += Modem_OnReceivedMessage;
        }

        public string COMPort
        {
            get { return _Modem.ComPort; }
        }

        public int BaudRate
        {
            get { return _Modem.BaudRate; }
        }

        public bool IsOpen
        {
            get { return _Modem.IsOpen; }
        }

        private MessageFormat_E _OriginalMessageFormat = MessageFormat_E.PDU;

        public void Open(string com_port, int baudrate)
        {
            _Modem.ComPort = com_port;
            _Modem.BaudRate = baudrate;
            _Modem.Open();

            // remember the original message format, and set message format to be PDU
            _OriginalMessageFormat = MessageFormat;
            if (_OriginalMessageFormat != MessageFormat_E.PDU)
                MessageFormat = MessageFormat_E.PDU;
        }

        public void Close()
        {
            if (_OriginalMessageFormat != MessageFormat_E.PDU)
                MessageFormat = _OriginalMessageFormat;

            _Modem.Close();
        }

        public string ExecuteATCommand(string cmd)
        {
            return _Modem.SendAT(cmd);
        }

        public string SMSCenterNumber
        {
            get { return _Modem.GetMsgCenterNo(); }
        }

        public void SendMessage(string recipient, string message)
        {
            _Modem.SendMsg(recipient, message, GSMMODEM.MsgType.AUSC2);
        }

        private static Result ParseATCommandOutput(string output)
        {
            /*
                sample 1:
                    +COPS: 0,0,"Hong Kong CSL"
                    OK

                sample 2:
                    +CSQ: 25, 99
                    OK
            */

            try
            {
                string[] lines = output.Split(new string[] { LineSeparator }, StringSplitOptions.RemoveEmptyEntries);
                if (lines.Length < 1)
                    throw new FormatException();

                int content_count = 0;
                string[] content = new string[lines.Length - 1];
                for (int i = 0; i < lines.Length - 1; i++)
                {
                    if (!lines[i].Contains(":"))
                        content[content_count++] = lines[i].Trim();
                    else
                        content[content_count++] = lines[i].Substring(lines[i].IndexOf(':') + 1).Trim();
                }

                return new Result(lines[lines.Length - 1], content);
            }
            catch (FormatException)
            {
                throw new FormatException(output);
            }
        }

        private void Modem_OnReceivedMessage(int msgIndex)
        {
            if (OnReceivedSMS != null)
                OnReceivedSMS(this, msgIndex);
        }

        private static Message.Message StringArrayToMessage(string[] msg)
        {
            int field = 0;
            string id = msg[field++];
            string msg_status = msg[field++];
            string sms_center = msg[field++];
            string number = msg[field++];
            string time_str = msg[field++];
            string message = msg[field++];

            DateTime time_utc = DateTime.ParseExact(time_str, "yyyyMMddHHmmss", System.Globalization.CultureInfo.InvariantCulture);

            Message.Message m;
            MessageStatus_E s = NumberToMessageStatus(int.Parse(msg_status));
            if (s == MessageStatus_E.Read)
                m = new Message.ReadMessage();
            else if (s == MessageStatus_E.Unread)
                m = new Message.UnreadMessage();
            else if (s == MessageStatus_E.Sent)
                m = new Message.SentMessage();
            else if (s == MessageStatus_E.Unsent)
                m = new Message.UnsentMessage();
            else
                throw new Exception("unsupported message status " + s.ToString());

            m.Set(int.Parse(id), number, time_utc, message);

            return m;
        }

        public Message.Message GetSMS(int id)
        {
            string[] result = _Modem.GetMsgByIndex(id);
            return StringArrayToMessage(result);
        }

        public Message.Message[] GetAllSMS(MessageStatus_E status)
        {
            int status_num = MessageStatusToNumber(status);
            string[][] msgs = _Modem.GetMsgByType(status_num);
            if (msgs == null)
                return null;

            List<Message.Message> list = new List<Message.Message>();
            foreach (string[] msg in msgs)
                list.Add(StringArrayToMessage(msg));

            return list.ToArray();
        }
#else // USEGSMMODEM
        private const string LineSeparator = Environment.NewLine;
        private SerialPort _sp = null;
        private Object _ReadLock = new Object();
#if DEBUG
        private string _AllBuffer = "";
#endif // DEBUG

        public Modem()
        {
            _sp = new SerialPort();
            _sp.DataReceived += SerialPort_DataReceived;
        }

        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (e.EventType == SerialData.Chars)
            {
#if DEBUG
                //lock (_ReadLock)
                //    _AllBuffer += _sp.ReadExisting();
#endif // DEBUG
                Console.WriteLine("data received");
            }
            else
                Console.WriteLine("eof");
        }

        public void Open(string comPort, int baudRate)
        {
            _sp.PortName = comPort;
            _sp.BaudRate = baudRate;
            _sp.NewLine = LineSeparator;
            _sp.ReadTimeout = 5000;
            _sp.RtsEnable = true;

            _sp.Open();

            string output = ExecuteATCommand("AT");
            Console.WriteLine(output);
        }

        public void Close()
        {
            _sp.Close();
        }

        public string COMPort
        {
            get { return _sp.PortName; }
        }

        public int BaudRate
        {
            get { return _sp.BaudRate; }
        }

        public bool IsOpened
        {
            get { return _sp.IsOpen; }
        }

        public string SMSCenterNumber
        {
            get
            {
                string cmd = "AT+CSCA?";
                /*
                sample output:
                AT+CSCA?
                +CSCA: "85290203449", 145
                OK
                */

                string output = ExecuteATCommand(cmd);
                string content = ParseATCommandOutput(output);
                string[] tokens = SplitResultTokens(content);
                if (tokens.Length != 2)
                    throw new FormatException(output);
                return tokens[0].Trim('\"');
            }
        }

        private static Message.Message ParseSMSFromOutput(string line1, string line2)
        {
            string[] tokens = SplitResultTokens(line1);

            int id = int.Parse(tokens[0]);

#if SMSPDULIB
            MessageStatus_E s = NumberToMessageStatus(int.Parse(tokens[1]));

            DateTime time;
            string to_from;
            string msg_content;
            switch (SMSPDULib.SMSBase.GetSMSType(line2))
            {
                case SMSPDULib.SMSType.SMS:
                    string encoded = line2;
                    SMSPDULib.SMS sms = new SMSPDULib.SMS();
                    SMSPDULib.SMS.Fetch(sms, ref encoded);
                    to_from = sms.PhoneNumber;
                    time = sms.ServiceCenterTimeStamp;
                    msg_content = sms.Message;
                    break;
                default:
                    return null;
            }
#else // SMSPDULIB
            MessageStatus_E s = StringToMessageStatus(tokens[1].Trim('\"'));

            string to_from = tokens[2].Trim('\"');
            string time_str = tokens[4].Trim('\"');
            // eg. 2017/9/10 17:56:32+32
            DateTime time = DateTime.ParseExact(time_str.Substring(0, time_str.IndexOf("+")), "yyyy/MM/dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

            List<byte> msg_content_bytes = new List<byte>();
            for (int j = 0; j < line2.Length; j += 2)
            {
                byte b = byte.Parse(line2.Substring(j, 2), System.Globalization.NumberStyles.HexNumber);
                msg_content_bytes.Add(b);
            }

            string msg_content = Encoding.BigEndianUnicode.GetString(msg_content_bytes.ToArray());
#endif // SMSPDULIB
            Message.Message msg;
            if (s == MessageStatus_E.Read)
                msg = new Message.ReadMessage();
            else if (s == MessageStatus_E.Unread)
                msg = new Message.UnreadMessage();
            else if (s == MessageStatus_E.Sent)
                msg = new Message.SentMessage();
            else if (s == MessageStatus_E.Unsent)
                msg = new Message.UnsentMessage();
            else
                throw new Exception("unsupported message status " + s.ToString());

            msg.Set(id, to_from, time, msg_content);

            return msg;
        }

        public Message.Message GetSMS(int id)
        {
            MessageFormat_E original_format = MessageFormat;
            //MessageCharset_E original_charset = MessageCharset;

            try
            {
#if SMSPDULIB
                if (original_format != MessageFormat_E.PDU)
                    MessageFormat = MessageFormat_E.PDU;
#else // SMSPDULIB
                // change to text format
                if (original_format != MessageFormat_E.Text)
                    MessageFormat = MessageFormat_E.Text;
                //if (original_charset != MessageCharset_E.UCS2)
                //    MessageCharset = MessageCharset_E.UCS2;
#endif // SMSPDULIB

                string output = ExecuteATCommand("AT+CMGR=" + id);
                string[] content_lines = ParseATCommandOutput(output);

                return ParseSMSFromOutput(content_lines[0], content_lines[1]);
            }
            finally
            {
#if SMSPDULIB
                if (original_format != MessageFormat_E.PDU)
                    MessageFormat = original_format;
#else // SMSPDULIB
                if (original_format != MessageFormat_E.Text)
                    MessageFormat = original_format;
                //if (original_charset != MessageCharset_E.UCS2)
                //    MessageCharset = original_charset;
#endif // SMSPDULIB
            }
        }

        public Message.Message[] GetAllSMS(MessageStatus_E status)
        {
            MessageFormat_E original_format = MessageFormat;
            //MessageCharset_E original_charset = MessageCharset;

            try
            {
#if SMSPDULIB
                if (original_format != MessageFormat_E.PDU)
                    MessageFormat = MessageFormat_E.PDU;

                int status_number = MessageStatusToNumber(status);

                string output = ExecuteATCommand("AT+CMGL=" + status_number);
#else // SMSPDULIB
                // change to text format
                if (original_format != MessageFormat_E.Text)
                    MessageFormat = MessageFormat_E.Text;
                //if (original_charset != MessageCharset_E.UCS2)
                //    MessageCharset = MessageCharset_E.UCS2;

                string status_txt = MessageStatusToString(status);

                string output = ExecuteATCommand("AT+CMGL=\"" + status_txt + "\"");
#endif // SMSPDULIB
                string[] content_lines = ParseATCommandOutput(output);

                Message.Message[] ret = new Message.Message[content_lines.Length / 2];
                for (int i = 0; i < content_lines.Length; i += 2)
                {
                    string line1 = content_lines[i];
                    string line2 = content_lines[i + 1];
                    Message.Message msg = ParseSMSFromOutput(line1, line2);
                    if (msg == null)
                        continue;
                    ret[i / 2] = msg;
                }
                return ret;
            }
            finally
            {
#if SMSPDULIB
                if (original_format != MessageFormat_E.PDU)
                    MessageFormat = original_format;
#else // SMSPDULIB
                if (original_format != MessageFormat_E.Text)
                    MessageFormat = original_format;
                //if (original_charset != MessageCharset_E.UCS2)
                //    MessageCharset = original_charset;
#endif // SMSPDULIB
            }
        }

        public string LastError
        {
            get
            {
                string output = ExecuteATCommand("AT+CME?");
                return output;
            }
        }

        public void SendMessage(string recipient_number, string content)
        {
            MessageFormat_E original_format = MessageFormat;

            try
            {
#if SMSPDULIB
                if (original_format != MessageFormat_E.PDU)
                    MessageFormat = MessageFormat_E.PDU;
                MessageCharset = MessageCharset_E.UCS2;

                //string sms_center = SMSCenterNumber;
                //sms_center = "+" + sms_center.TrimStart('+');

                SMSPDULib.SMS sms = new SMSPDULib.SMS();
                sms.Direction = SMSPDULib.SMSDirection.Submited;
                sms.PhoneNumber = recipient_number;
                sms.ValidityPeriod = new TimeSpan(4, 0, 0, 0);
                sms.Message = content;
                string encoded = sms.Compose(SMSPDULib.SMS.SMSEncoding.UCS2);

                // start
                SendToBuffer("AT+CMGS=" + ((encoded.Length - 2) / 2) + LineSeparator);

                // wait for ">"
                while (true)
                {
                    string output = ReadBuffer();
                    if (output.EndsWith(LineSeparator + ">"))
                        break;
                    System.Threading.Thread.Sleep(100);
                }

                // send content
                SendToBuffer(encoded);

                // send Ctrl+Z
                SendToBuffer("" + (char)0x1a);

                // wait for response
                while (true)
                {
                    string output = ReadBuffer();
                    if (IsResponseComplete(output))
                    {
                        try
                        {
                            ParseATCommandOutput(output).GetOrThrow();
                        }
                        catch {
                            Console.WriteLine(LastError);
                            throw;
                        }
                        return;
                    }
                }
#else // SMSPDULIB
                if (original_format != MessageFormat_E.Text)
                    MessageFormat = MessageFormat_E.Text;

                // start
                SendToBuffer("AT+CMGS=\"" + recipient_number + "\"");

                // wait for ">"
                while (true)
                {
                    string output = ReadBuffer();
                    if (output.EndsWith(LineSeparator + ">"))
                        break;
                    System.Threading.Thread.Sleep(100);
                }

                // send content
#if kill
                byte[] b = Encoding.BigEndianUnicode.GetBytes(content);
                //byte[] b = Encoding.Unicode.GetBytes(content);
                string encoded_content = "";
                foreach (byte bb in b)
                    encoded_content += bb.ToString("X02");
                SendATCommand(encoded_content);
#else // kill
                SendToBuffer(content);
#endif // kill

                // send Ctrl+Z
                SendToBuffer("" + (char)0x1a);

                // wait for response
                while (true)
                {
                    string output = ReadBuffer();
                    if (IsResponseComplete(output))
                    {
                        ParseATCommandOutput(output).GetOrThrow();
                        return;
                    }
                }
#endif // SMSPDULIB
            }
            finally
            {
#if SMSPDULIB
                if (original_format != MessageFormat_E.PDU)
                    MessageFormat = original_format;
#else // SMSPDULIB
                if (original_format != MessageFormat_E.Text)
                    MessageFormat = original_format;
#endif // SMSPDULIB
            }
        }

        public void Call(string recipient_number)
        {
            string output = ExecuteATCommand("ATD" + recipient_number + ";");
            ParseATCommandOutput(output).GetOrThrow();
        }

        public void Hangup()
        {
            string output = ExecuteATCommand("ATH;");
            ParseATCommandOutput(output).GetOrThrow();
        }

        private void ClearBuffer()
        {
            lock (_ReadLock)
                _sp.DiscardInBuffer();
        }

        private void SendToBuffer(string cmd)
        {
            _sp.Write(cmd);
        }

        private string ReadBuffer()
        {
            lock (_ReadLock)
                return _sp.ReadExisting().Trim();
        }

        private static bool IsResponseComplete(string output)
        {
            return output.EndsWith(LineSeparator + "ERROR") || output.EndsWith(LineSeparator + "OK");
        }

        private string ExecuteATCommand(ATCommand cmd)
        {
            ClearBuffer();
            SendToBuffer(cmd.CommandString + LineSeparator);

            // wait until it shows "OK or "ERROR"
            while (true)
            {
                string curr = ReadBuffer();
                if (IsResponseComplete(curr))
                    return curr;
                System.Threading.Thread.Sleep(100);
            }
        }

        public static string[] GetConnectedCOMPorts()
        {
            string[] ret = (from p in SerialPort.GetPortNames() where Regex.IsMatch(p, "COM[0-9]+") select p).ToArray();
            Array.Sort(ret, (p1, p2) => { return int.Parse(p1.Substring(3)) - int.Parse(p2.Substring(3)); });
            return ret;
        }

        public string ExecuteATCommand(string cmd)
        {
            return ExecuteATCommand(new ATCommand(cmd));
        }

        private static Result ParseATCommandOutput(string output)
        {
            /*
                sample 1:
                    AT+COPS?
                    +COPS: 0,0,"Hong Kong CSL"
                    OK

                sample 2:
                    AT+CSQ
                    +CSQ: 25, 99
                    OK
            */

            try
            {
                string[] lines = output.Split(new string[] { LineSeparator }, StringSplitOptions.RemoveEmptyEntries);
                if (lines.Length < 2)
                    throw new FormatException();

                int content_count = 0;
                string[] content = new string[lines.Length - 2];
                for (int i = 1; i < lines.Length - 1; i++)
                {
                    if (!lines[i].Contains(":"))
                        content[content_count++] = lines[i].Trim();
                    else
                        content[content_count++] = lines[i].Substring(lines[i].IndexOf(':') + 1).Trim();
                }

                return new Result(lines[lines.Length - 1], content);
            }
            catch (FormatException)
            {
                throw new FormatException(output);
            }
        }
#endif // USEGSMMODEM
        private static string[] SplitResultTokens(string result)
        {
            List<string> tokens = new List<string>();

            int cursor = 0;
            while (cursor < result.Length)
            {
                // ignore all leading spaces
                if (result[cursor] == ' ')
                {
                    cursor++;
                    continue;
                }

                int pos;
                if (result[cursor] == '\"')
                    pos = result.IndexOf('\"', cursor + 1) + 1;
                else
                    pos = result.IndexOf(',', cursor);

                string t = pos == -1 ? result.Substring(cursor) : result.Substring(cursor, pos - cursor);
                tokens.Add(t);
                cursor += t.Length;

                // ignore all trailing spaces
                while (cursor < result.Length && result[cursor] != ',')
                    cursor++;

                cursor++;   // jump over the comma
            }

            return tokens.ToArray();
        }

        public int SignalStrength
        {
            get
            {
                string cmd = "AT+CSQ";
                // sample output:
                // AT+CSQ
                // +CSQ: 25, 99
                // OK

                string output = ExecuteATCommand(cmd);
                string content = ParseATCommandOutput(output);
                if (!content.Contains(","))
                    throw new FormatException(output);

                int ret;
                string[] tokens = SplitResultTokens(content);
                if (tokens.Length != 2 || !int.TryParse(tokens[0], out ret))
                    throw new FormatException(output);

                return ret;
            }
        }

        public string Operator
        {
            get
            {
                string cmd = "AT+COPS?";
                // sample output:
                // AT+COPS?
                // +COPS: 0,0,"Hong Kong CSL"
                // OK

                string output = ExecuteATCommand(cmd);
                string content = ParseATCommandOutput(output);
                string[] tokens = SplitResultTokens(content);
                if (tokens.Length < 3)
                    return "";
                return tokens[2].Trim('\"');
            }
        }

        public void Call(string recipient_number)
        {
            string output = ExecuteATCommand("ATD" + recipient_number + ";");
            ParseATCommandOutput(output).GetOrThrow();
        }

        public void Hangup()
        {
            string output = ExecuteATCommand("ATH;");
            ParseATCommandOutput(output).GetOrThrow();
        }

        public MessageFormat_E MessageFormat
        {
            get
            {
                string cmd = "AT+CMGF?";
                string output = ExecuteATCommand(cmd);
                string content = ParseATCommandOutput(output);
                if (content == "0")
                    return MessageFormat_E.PDU;
                else if (content == "1")
                    return MessageFormat_E.Text;
                else
                    throw new NotImplementedException("unsupported message format " + content);
            }
            set
            {
                string cmd = "AT+CMGF=";
                if (value == MessageFormat_E.PDU)
                    cmd += "0";
                else if (value == MessageFormat_E.Text)
                    cmd += "1";
                else
                    throw new NotImplementedException("unsupported message format " + value.ToString());

                string output = ExecuteATCommand(cmd);
                Result result = ParseATCommandOutput(output);
                result.GetOrThrow();
            }
        }

        public MessageCharset_E MessageCharset
        {
            get
            {
                string cmd = "AT+CSCS?";
                string output = ExecuteATCommand(cmd);
                string content = ParseATCommandOutput(output).GetOrThrow().Trim('\"');
                if (content == "UCS2")
                    return MessageCharset_E.UCS2;
                else if (content == "IRA")
                    return MessageCharset_E.IRA;
                else if (content == "8859-1")
                    return MessageCharset_E.ISO8859;
                else if (content == "PCCP437")
                    return MessageCharset_E.PCCP437;
                else if (content == "HEX")
                    return MessageCharset_E.HEX;
                else
                    throw new NotImplementedException("unsupported message charset " + content);
            }
            set
            {
                string cmd = "AT+CSCS=\"";
                if (value == MessageCharset_E.UCS2)
                    cmd += "UCS2";
                else if (value == MessageCharset_E.IRA)
                    cmd += "IRA";
                else if (value == MessageCharset_E.ISO8859)
                    cmd += "8859-1";
                else if (value == MessageCharset_E.PCCP437)
                    cmd += "PCCP437";
                else if (value == MessageCharset_E.HEX)
                    cmd += "HEX";
                else
                    throw new NotImplementedException("unsupported message charset " + value.ToString());
                cmd += "\"";

                string output = ExecuteATCommand(cmd);
                ParseATCommandOutput(output).GetOrThrow();
            }
        }

#if SMSPDULIB
        private static int MessageStatusToNumber(MessageStatus_E status)
        {
            if (status == MessageStatus_E.All)
                return 4;
            else if (status == MessageStatus_E.Unread)
                return 0;
            else if (status == MessageStatus_E.Read)
                return 1;
            else if (status == MessageStatus_E.Unsent)
                return 2;
            else if (status == MessageStatus_E.Sent)
                return 3;
            else
                throw new Exception("unsupported status " + status.ToString());
        }

        private static MessageStatus_E NumberToMessageStatus(int num)
        {
            if (num == 4)
                return MessageStatus_E.All;
            else if (num == 0)
                return MessageStatus_E.Unread;
            else if (num == 1)
                return MessageStatus_E.Read;
            else if (num == 2)
                return MessageStatus_E.Unsent;
            if (num == 3)
                return MessageStatus_E.Sent;
            else
                throw new Exception("unsupported status string " + num);
        }
#else // SMSPDULIB
        private static string MessageStatusToString(MessageStatus_E status)
        {
            if (status == MessageStatus_E.All)
                return "ALL";
            else if (status == MessageStatus_E.Unread)
                return "REC UNREAD";
            else if (status == MessageStatus_E.Read)
                return "REC READ";
            else if (status == MessageStatus_E.Unsent)
                return "STO UNSENT";
            else if (status == MessageStatus_E.Sent)
                return "STO SENT";
            else
                throw new Exception("unsupported status " + status.ToString());
        }

        private static MessageStatus_E StringToMessageStatus(string str)
        {
            if (str == "ALL")
                return MessageStatus_E.All;
            else if (str == "REC UNREAD")
                return MessageStatus_E.Unread;
            else if (str == "REC READ")
                return MessageStatus_E.Read;
            else if (str == "STO UNSENT")
                return MessageStatus_E.Unsent;
            if (str == "STO SENT")
                return MessageStatus_E.Sent;
            else
                throw new Exception("unsupported status string " + str);
        }
#endif // SMSPDULIB
    }
}
