﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test
{
    public partial class Form1 : Form
    {
        private ModemPool2.Modem _Modem = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void UpdateStatus(string msg)
        {
            this.BeginInvoke((MethodInvoker)delegate ()
            {
                StatusLabel.Text = msg;
                Application.DoEvents();
            });
        }

        private void OpenButton_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            if (_Modem == null)
            {
                string com_port = COMPortTextBox.Text.Trim();
                int baudrate = int.Parse(BaudrateTextBox.Text.Trim());

                UpdateStatus("opening " + com_port + " " + baudrate);
                _Modem = new ModemPool2.Modem();
                _Modem.OnReceivedSMS += Modem_OnReceivedSMS;
                _Modem.Open(com_port, baudrate);

                UpdateStatus("updating data");
                UpdateData();

                UpdateStatus("updating sms");
                UpdateSMS();

                UpdateStatus("opened");

                OpenButton.Text = "Close";
                COMPortTextBox.Enabled = BaudrateTextBox.Enabled = false;
            }
            else
            {
                UpdateStatus("closing");
                _Modem.Close();
                _Modem = null;
                UpdateData();
                UpdateSMS();
                UpdateStatus("closed");
                OpenButton.Text = "Open";
                COMPortTextBox.Enabled = BaudrateTextBox.Enabled = true;
            }

            Cursor.Current = Cursors.Default;
        }

        private void Modem_OnReceivedSMS(ModemPool2.Modem modem, int sms_id)
        {
            ModemPool2.Message.IncomingMessage msg = modem.GetSMS(sms_id) as ModemPool2.Message.IncomingMessage;
            Console.WriteLine(msg.Id);
            Console.WriteLine(msg.Sender);
            Console.WriteLine(msg.ReceiveTime);
            Console.WriteLine(msg.Content);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_Modem != null)
            {
                e.Cancel = true;
                MessageBox.Show("close the device");
                return;
            }
        }

        private void UpdateData()
        {
            int row = 0;
            Func<string, string, ListViewItem> append_row = (data, val) =>
            {
                ListViewItem itm;
                if (InfoListView.Items.Count <= row)
                    itm = InfoListView.Items.Add(new ListViewItem(new string[InfoListView.Columns.Count]));
                else
                    itm = InfoListView.Items[row];
                itm.SubItems[0].Text = data;
                itm.SubItems[1].Text = val;
                row++;
                return itm;
            };

            if (_Modem != null)
            {
                append_row("COM Port", _Modem.COMPort);
                append_row("Baudrate", _Modem.BaudRate.ToString());
                append_row("Signal Strength", _Modem.SignalStrength.ToString());
                append_row("Operator", _Modem.Operator);
                append_row("SMS Center", _Modem.SMSCenterNumber);
                append_row("Message Format", _Modem.MessageFormat.ToString());
                append_row("Message Charset", _Modem.MessageCharset.ToString());
            }

            while (InfoListView.Items.Count > row)
                InfoListView.Items.RemoveAt(InfoListView.Items.Count - 1);
        }

        private void ExecuteButton_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            string cmd = ATCommandTextBox.Text;
            Cursor.Current = Cursors.Default;
            MessageBox.Show(_Modem.ExecuteATCommand(cmd), cmd);
        }

        private void RefreshDataLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            UpdateData();
            Cursor.Current = Cursors.Default;
        }

        private void UpdateSMS()
        {
            ModemPool2.Message.Message[] messages = _Modem != null ? _Modem.GetAllSMS(ModemPool2.Modem.MessageStatus_E.All) : null;

            int row = 0;
            Func<int, string, DateTime, string, string, ListViewItem> append_row = (id, status, time, to_from, content) =>
            {
                ListViewItem itm;
                if (SMSListView.Items.Count <= row)
                    itm = SMSListView.Items.Add(new ListViewItem(new string[SMSListView.Columns.Count]));
                else
                    itm = SMSListView.Items[row];

                int col = 0;
                itm.SubItems[col++].Text = id.ToString();
                itm.SubItems[col++].Text = status;
                itm.SubItems[col++].Text = time.ToString("yyyyMMdd HHmmss");
                itm.SubItems[col++].Text = to_from;
                itm.SubItems[col++].Text = content;

                row++;
                return itm;
            };

            if (messages != null)
            {
                foreach (ModemPool2.Message.Message msg in messages)
                {
                    if (msg is ModemPool2.Message.SentMessage)
                    {
                        ModemPool2.Message.SentMessage sent = msg as ModemPool2.Message.SentMessage;
                        append_row(sent.Id, "Out/Sent", sent.SendTime, sent.Receiver, sent.Content);
                    }
                    else if (msg is ModemPool2.Message.UnsentMessage)
                    {
                        ModemPool2.Message.UnsentMessage unsent = msg as ModemPool2.Message.UnsentMessage;
                        append_row(unsent.Id, "Out/Unent", unsent.SendTime, unsent.Receiver, unsent.Content);
                    }
                    if (msg is ModemPool2.Message.ReadMessage)
                    {
                        ModemPool2.Message.ReadMessage read = msg as ModemPool2.Message.ReadMessage;
                        append_row(read.Id, "In/Read", read.ReceiveTime, read.Sender, read.Content);
                    }
                    else if (msg is ModemPool2.Message.UnreadMessage)
                    {
                        ModemPool2.Message.UnreadMessage unread = msg as ModemPool2.Message.UnreadMessage;
                        append_row(unread.Id, "In/Unread", unread.ReceiveTime, unread.Sender, unread.Content);
                    }
                    else
                        append_row(msg.Id, "Unknown", DateTime.MinValue, "Unknown", msg.Content);
                }
            }

            while (SMSListView.Items.Count > row)
                SMSListView.Items.RemoveAt(SMSListView.Items.Count - 1);
        }

        private void RefreshSMSLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            UpdateSMS();
            Cursor.Current = Cursors.Default;
        }

        private void SendSMSButton_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            string recipient = SMSRecipientTextBox.Text.Trim();
            string msg = string.Join(Environment.NewLine, SMSMessageTextBox.Lines);
            _Modem.SendMessage(recipient, msg);
            MessageBox.Show("done");
            Cursor.Current = Cursors.Default;
        }

        private void CallButton_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (CallButton.Text == "Call")
            {
                CallRecipientTextBox.Enabled = false;
                CallButton.Text = "Hangup";

                _Modem.Call(CallRecipientTextBox.Text);
            }
            else
            {
                _Modem.Hangup();

                CallRecipientTextBox.Enabled = true;
                CallButton.Text = "Call";
            }
            Cursor.Current = Cursors.Default;
        }
    }
}
