﻿namespace Test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ExecuteButton = new System.Windows.Forms.Button();
            this.ATCommandTextBox = new System.Windows.Forms.TextBox();
            this.RefreshSMSLinkLabel = new System.Windows.Forms.LinkLabel();
            this.SMSListView = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.RefreshDataLinkLabel = new System.Windows.Forms.LinkLabel();
            this.InfoListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.StatusLabel = new System.Windows.Forms.Label();
            this.OpenButton = new System.Windows.Forms.Button();
            this.BaudrateTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.COMPortTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SMSRecipientTextBox = new System.Windows.Forms.TextBox();
            this.SendSMSButton = new System.Windows.Forms.Button();
            this.SMSMessageTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.CallButton = new System.Windows.Forms.Button();
            this.CallRecipientTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "COM Port (eg. COM1)";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ExecuteButton);
            this.groupBox2.Controls.Add(this.ATCommandTextBox);
            this.groupBox2.Location = new System.Drawing.Point(14, 379);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(288, 53);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "AT Command";
            // 
            // ExecuteButton
            // 
            this.ExecuteButton.Location = new System.Drawing.Point(220, 12);
            this.ExecuteButton.Name = "ExecuteButton";
            this.ExecuteButton.Size = new System.Drawing.Size(58, 35);
            this.ExecuteButton.TabIndex = 11;
            this.ExecuteButton.Text = "Execute";
            this.ExecuteButton.UseVisualStyleBackColor = true;
            this.ExecuteButton.Click += new System.EventHandler(this.ExecuteButton_Click);
            // 
            // ATCommandTextBox
            // 
            this.ATCommandTextBox.Location = new System.Drawing.Point(6, 21);
            this.ATCommandTextBox.Name = "ATCommandTextBox";
            this.ATCommandTextBox.Size = new System.Drawing.Size(208, 22);
            this.ATCommandTextBox.TabIndex = 10;
            this.ATCommandTextBox.Text = "AT+CSQ";
            // 
            // RefreshSMSLinkLabel
            // 
            this.RefreshSMSLinkLabel.AutoSize = true;
            this.RefreshSMSLinkLabel.Location = new System.Drawing.Point(6, 17);
            this.RefreshSMSLinkLabel.Name = "RefreshSMSLinkLabel";
            this.RefreshSMSLinkLabel.Size = new System.Drawing.Size(41, 12);
            this.RefreshSMSLinkLabel.TabIndex = 14;
            this.RefreshSMSLinkLabel.TabStop = true;
            this.RefreshSMSLinkLabel.Text = "Refresh";
            this.RefreshSMSLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.RefreshSMSLinkLabel_LinkClicked);
            // 
            // SMSListView
            // 
            this.SMSListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7});
            this.SMSListView.FullRowSelect = true;
            this.SMSListView.GridLines = true;
            this.SMSListView.Location = new System.Drawing.Point(6, 32);
            this.SMSListView.Name = "SMSListView";
            this.SMSListView.Size = new System.Drawing.Size(504, 126);
            this.SMSListView.TabIndex = 13;
            this.SMSListView.UseCompatibleStateImageBehavior = false;
            this.SMSListView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Id";
            this.columnHeader3.Width = 30;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Status";
            this.columnHeader4.Width = 100;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Time";
            this.columnHeader5.Width = 100;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Sender/Receiver";
            this.columnHeader6.Width = 100;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Content";
            this.columnHeader7.Width = 150;
            // 
            // RefreshDataLinkLabel
            // 
            this.RefreshDataLinkLabel.AutoSize = true;
            this.RefreshDataLinkLabel.Location = new System.Drawing.Point(11, 18);
            this.RefreshDataLinkLabel.Name = "RefreshDataLinkLabel";
            this.RefreshDataLinkLabel.Size = new System.Drawing.Size(41, 12);
            this.RefreshDataLinkLabel.TabIndex = 12;
            this.RefreshDataLinkLabel.TabStop = true;
            this.RefreshDataLinkLabel.Text = "Refresh";
            this.RefreshDataLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.RefreshDataLinkLabel_LinkClicked);
            // 
            // InfoListView
            // 
            this.InfoListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.InfoListView.FullRowSelect = true;
            this.InfoListView.GridLines = true;
            this.InfoListView.Location = new System.Drawing.Point(8, 33);
            this.InfoListView.Name = "InfoListView";
            this.InfoListView.Size = new System.Drawing.Size(270, 229);
            this.InfoListView.TabIndex = 8;
            this.InfoListView.UseCompatibleStateImageBehavior = false;
            this.InfoListView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Data";
            this.columnHeader1.Width = 100;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Value";
            this.columnHeader2.Width = 140;
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Location = new System.Drawing.Point(318, 58);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(137, 12);
            this.StatusLabel.TabIndex = 7;
            this.StatusLabel.Text = "Click [Open] to open device";
            // 
            // OpenButton
            // 
            this.OpenButton.Location = new System.Drawing.Point(320, 9);
            this.OpenButton.Name = "OpenButton";
            this.OpenButton.Size = new System.Drawing.Size(65, 46);
            this.OpenButton.TabIndex = 6;
            this.OpenButton.Text = "Open";
            this.OpenButton.UseVisualStyleBackColor = true;
            this.OpenButton.Click += new System.EventHandler(this.OpenButton_Click);
            // 
            // BaudrateTextBox
            // 
            this.BaudrateTextBox.Location = new System.Drawing.Point(150, 25);
            this.BaudrateTextBox.Name = "BaudrateTextBox";
            this.BaudrateTextBox.Size = new System.Drawing.Size(152, 22);
            this.BaudrateTextBox.TabIndex = 5;
            this.BaudrateTextBox.Text = "115200";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(148, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "Baudrate (eg. 9600, or 115200)";
            // 
            // COMPortTextBox
            // 
            this.COMPortTextBox.Location = new System.Drawing.Point(14, 25);
            this.COMPortTextBox.Name = "COMPortTextBox";
            this.COMPortTextBox.Size = new System.Drawing.Size(111, 22);
            this.COMPortTextBox.TabIndex = 3;
            this.COMPortTextBox.Text = "COM9";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 12);
            this.label4.TabIndex = 15;
            this.label4.Text = "Recipient number";
            // 
            // SMSRecipientTextBox
            // 
            this.SMSRecipientTextBox.Location = new System.Drawing.Point(100, 17);
            this.SMSRecipientTextBox.Name = "SMSRecipientTextBox";
            this.SMSRecipientTextBox.Size = new System.Drawing.Size(101, 22);
            this.SMSRecipientTextBox.TabIndex = 16;
            this.SMSRecipientTextBox.Text = "64500366";
            // 
            // SendSMSButton
            // 
            this.SendSMSButton.Location = new System.Drawing.Point(128, 142);
            this.SendSMSButton.Name = "SendSMSButton";
            this.SendSMSButton.Size = new System.Drawing.Size(73, 34);
            this.SendSMSButton.TabIndex = 17;
            this.SendSMSButton.Text = "Send";
            this.SendSMSButton.UseVisualStyleBackColor = true;
            this.SendSMSButton.Click += new System.EventHandler(this.SendSMSButton_Click);
            // 
            // SMSMessageTextBox
            // 
            this.SMSMessageTextBox.Location = new System.Drawing.Point(6, 58);
            this.SMSMessageTextBox.Multiline = true;
            this.SMSMessageTextBox.Name = "SMSMessageTextBox";
            this.SMSMessageTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.SMSMessageTextBox.Size = new System.Drawing.Size(195, 78);
            this.SMSMessageTextBox.TabIndex = 18;
            this.SMSMessageTextBox.Text = "hello";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 43);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 12);
            this.label5.TabIndex = 19;
            this.label5.Text = "Message";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.SMSRecipientTextBox);
            this.groupBox1.Controls.Add(this.SMSMessageTextBox);
            this.groupBox1.Controls.Add(this.SendSMSButton);
            this.groupBox1.Location = new System.Drawing.Point(320, 96);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(209, 186);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Send SMS";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.CallButton);
            this.groupBox3.Controls.Add(this.CallRecipientTextBox);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Location = new System.Drawing.Point(320, 343);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(209, 89);
            this.groupBox3.TabIndex = 21;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Voice Call";
            // 
            // CallButton
            // 
            this.CallButton.Location = new System.Drawing.Point(128, 43);
            this.CallButton.Name = "CallButton";
            this.CallButton.Size = new System.Drawing.Size(73, 34);
            this.CallButton.TabIndex = 2;
            this.CallButton.Text = "Call";
            this.CallButton.UseVisualStyleBackColor = true;
            this.CallButton.Click += new System.EventHandler(this.CallButton_Click);
            // 
            // CallRecipientTextBox
            // 
            this.CallRecipientTextBox.Location = new System.Drawing.Point(99, 15);
            this.CallRecipientTextBox.Name = "CallRecipientTextBox";
            this.CallRecipientTextBox.Size = new System.Drawing.Size(102, 22);
            this.CallRecipientTextBox.TabIndex = 1;
            this.CallRecipientTextBox.Text = "27454466";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "Recipient number";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.InfoListView);
            this.groupBox4.Controls.Add(this.RefreshDataLinkLabel);
            this.groupBox4.Location = new System.Drawing.Point(14, 96);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(288, 274);
            this.groupBox4.TabIndex = 22;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Device Properties";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.SMSListView);
            this.groupBox5.Controls.Add(this.RefreshSMSLinkLabel);
            this.groupBox5.Location = new System.Drawing.Point(14, 438);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(516, 169);
            this.groupBox5.TabIndex = 23;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Messages";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 619);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.COMPortTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.BaudrateTextBox);
            this.Controls.Add(this.OpenButton);
            this.Controls.Add(this.StatusLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button OpenButton;
        private System.Windows.Forms.TextBox BaudrateTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox COMPortTextBox;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.ListView InfoListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button ExecuteButton;
        private System.Windows.Forms.TextBox ATCommandTextBox;
        private System.Windows.Forms.LinkLabel RefreshDataLinkLabel;
        private System.Windows.Forms.LinkLabel RefreshSMSLinkLabel;
        private System.Windows.Forms.ListView SMSListView;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox SMSRecipientTextBox;
        private System.Windows.Forms.Button SendSMSButton;
        private System.Windows.Forms.TextBox SMSMessageTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button CallButton;
        private System.Windows.Forms.TextBox CallRecipientTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
    }
}

