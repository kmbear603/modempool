# ModemPool
A C# Library to operate the modem pool 惠通工業無線 using AT commands.

This library is created because there is no good known C# library for the purpose of sending/receiving SMS.

## Projects in solution
* ModemPool2
* Test - for development purpose

## Acknowledgement
A part of the project (PDU encoding) is modified from the link below:
http://www.cnblogs.com/Engin/archive/2010/11/14/1877040.html

## Author
kmbear603@gmail.com
